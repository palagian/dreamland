import React from "react"
import "./index.css"
import logoinst from "./img/instagram-logo.png"

const Header = () => {
    return (
        <>
            <div className="header">
                <div className="logo">Dreamland</div>
                <div className="adress"><a href="https://goo.gl/maps/fcgTE7Pho3DnVbtN8" target="_blank">Киев, ул. Раисы Окипной, 26</a></div>
                <div className="mob"><a href="tel:+380997271711">+38 099-727-17-11</a></div>
                <div className="insta"><a href="https://www.instagram.com/dreamland.club/?hl=ru" target="_blank"><img alt="instagram" src={logoinst}></img></a></div>
            </div>
            <div className="nav">
                <ul>
                    <li className="about"><a href="#main">Про нас</a></li>
                    <li className="price"><a href="#priceList">Цены</a></li>
                    <li className="team2"><a href="#team">Команда</a></li>
                    <li className="boat"><a href="#boat">Катер</a></li>
                    <li className="contact"><a href="#contacts">Контакты</a></li>
                </ul>
            </div>
        </>
    )
}

export default Header