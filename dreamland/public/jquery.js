$(document).ready(function () {

    // создаем правило для валидации поля е-мейла
    const validate = document.querySelector("#exampleFormControlInput1");
    const text = document.querySelector("#exampleFormControlTextarea1")
    const error = document.querySelector(".error");
    let validFlag = false;

    $(validate).on("change", () => {
        if (/[ˆa-z0-9._]+@[a-z0-9.-]+\.[a-z]+$/.test(validate.value)) {
            validFlag = true;
            error.innerText = "";
        } else {
            error.innerText = "емейл указан неверно!";
            validFlag = false;
        }
    })

    // создаем ajax запрос для отправки сообщения через поле ввода
    $("#form").on("submit", function (e) {
        e.preventDefault();
        if (validFlag && text.value !== "") {
            $.ajax({
                url: 'sendForm.php',
                method: 'post',
                dataType: 'json',
                data: $(this).serialize(),
                success: function (data) {
                    if (data.res == "send") {
                        $('.message2').html("Ваше сообщение отправлено!");
                    } else {
                        $('.message2').html("Ваше сообщение не отправлено!");
                    }
                }
            });
        } else {
            alert("Форма заполнена неверно!")
            validFlag = false;
        }
    });
})