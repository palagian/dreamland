window.onload = () => {
    // создаем слайдшоу (3 разных на странице)
    let slideIndex = 0;
    let slideIndex2 = 0;
    let slideIndex3 = 0;

    function showSlides() {
        let slides = document.getElementsByClassName("slide");
        for (let i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slideIndex++;
        if (slideIndex > slides.length) { slideIndex = 1 }
        slides[slideIndex - 1].style.display = "block";
        setTimeout(showSlides, 3000);
    }
    showSlides();

    function showSlides2() {
        let slides2 = document.getElementsByClassName("slide2");
        for (let a = 0; a < slides2.length; a++) {
            slides2[a].style.display = "none";
        }
        slideIndex2++;
        if (slideIndex2 > slides2.length) { slideIndex2 = 1 }
        slides2[slideIndex2 - 1].style.display = "block";
        setTimeout(showSlides2, 3500);
    }
    showSlides2();

    function showSlides3() {
        let slides3 = document.getElementsByClassName("slide3");
        for (let b = 0; b < slides3.length; b++) {
            slides3[b].style.display = "none";
        }
        slideIndex3++;
        if (slideIndex3 > slides3.length) { slideIndex3 = 1 }
        slides3[slideIndex3 - 1].style.display = "block";
        setTimeout(showSlides3, 3500);
    }
    showSlides3();

    //присваиваем кнопке "Записаться" событие на клик
    const btn = document.querySelector(".btn");

    btn.addEventListener("click", () => {
        const tab = window.open('https://www.instagram.com/dreamland.club/?hl=ru', '_blank');
    })

}